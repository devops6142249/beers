import os
import json
import glob
import logging
import argparse
from flask import Flask, request, jsonify

logger = logging.getLogger(__name__)
app = Flask(__name__)

parser = argparse.ArgumentParser("Hybrid gateway config")
parser.add_argument("--port", type=int, default=False, required=True)

@app.route("/", methods = ["GET"]) 
def home(): 
    if(request.method == "GET"): 
        data = "hello world"
        return jsonify({"data": data})

def return_mess(code, mes):
    return jsonify({"message": f"{mes}","status":code})

class shopCart():
    def __init__(self):
        self.root = "./"
        self.item_path = os.path.join(self.root, "beers.json")
        self.n_count = 0
        self.type_list = ["offset", "id", "name", "tagline", "food_pairing", "price"]
        self.name_list = set()

        f = open(self.item_path, "r")
        self.all_config = json.loads(f.read())

    def init_config(self, item_path):
        self.n_count = len(self.all_config)
        
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            self.name_list.add(db_name)

    def return_all(self):
        return self.all_config

    def return_type_list(self):
        return self.type_list

    def check_id(self, id):
        if id <= 25 and id >= 0:
            return True
        else:
            return False
    
    def check_name(self, name):
        self.init_config(self.item_path)
        if name in self.name_list:
            return True
        else:
            return False

    def get_name_list(self):
        self.init_config(self.item_path)
        return self.name_list
    
    def info_by_id(self, id):
        info = self.all_config[id - 1]
        return info

    def info_by_name(self, name):
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            if name in db_name.split():
                return content
            else:
                pass

    def info_by_price(self, price):
        return_list = []
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            db_price = content["price"]
            if int(price) == int(db_price):
                return_list.append(db_name)
        
        return return_list

    def price_range(self):
        return_range = []
        for i, content in enumerate(self.all_config):
            db_price = content["price"]
            return_range.append(db_price)

        return [min(return_range), max(return_range)]

    def info_by_tagline(self, tagline):
        list_content = {}
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            db_tagline = content["tagline"]
            for j, temp in enumerate(db_tagline.split()):
                if tagline == temp.strip(".") or tagline == temp:
                    list_content[i] = content
        
        return list_content

    def tagline_suggestion(self):
        return_suggestion = []
        for i, content in enumerate(self.all_config):
            db_tagline = content["tagline"]
            return_suggestion.append(db_tagline)

        return return_suggestion
    
    def info_by_foodpairing(self, food):
        list_content = {}
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            db_food = content["food_pairing"]
            for j, temp in enumerate(db_food):
                keywords = temp.split()
                if food in keywords:
                    list_content[i] = content
                else:
                    pass
        
        return list_content

    def food_suggestion(self):
        return_suggestion = []
        for i, content in enumerate(self.all_config):
            db_tagline = content["food_pairing"]
            return_suggestion.append(db_tagline)

        return return_suggestion


shopCart_app = shopCart()
args = parser.parse_args()

tasks = {}
def task(task_fn):
    tasks[task_fn.__name__] = task_fn

@task
def search_offset(offset):
    info = shopCart_app.return_all()
    try:
        start = int(offset.split(":")[0])
        end = int(offset.split(":")[1])
    except Exception as e:
        mess = return_mess(404, "Wrong input format. An example of correct format: 0:10")
        return mess

    try:
        assert start > 0
        assert end < 25
    except AssertionError as AE:
        mess = return_mess(404, "invalid values for format [start_number:end_number], where start_number has to be positive and end_number has be to smaller than 25.")
        return mess

    mess = return_mess(200, info[start-1:end])
    return mess

@task
def search_id(id):
    if id == "all":
        info = shopCart_app.return_all()
        mess = return_mess(200, info)
        return mess
    else:
        try:
            id = int(id)
            if shopCart_app.check_id(id):
                pass
            else:
                mess = return_mess(400, "ID is not valid.")
                return mess
            
            info = shopCart_app.info_by_id(id)

            mess = return_mess(200, info)
            return mess
        except ValueError as VE:
            mess = return_mess(404, "Invalid input. Please try again.")
            return mess

@task
def search_name(name):    
    info = shopCart_app.info_by_name(name)

    if info is None:
        name_list = shopCart_app.get_name_list()
        mess = return_mess(400, f"Name is not valid. List of available beer: {name_list}")
        return mess
    else:
        pass

    mess = return_mess(200, info)
    return mess

@task
def search_price(price):
    price = float(price)
    info = shopCart_app.info_by_price(price)
    if info == []:
        price_range = shopCart_app.price_range()
        mess = return_mess(200, f"Here is the price range: {price_range}")
        return mess
    else:
        mess = return_mess(200, f"List of beers with your suggest price: {info}")
        return mess

@task
def search_tagline(tagline):
    info = shopCart_app.info_by_tagline(tagline)
    if info == {}:
        suggestion = shopCart_app.tagline_suggestion()
        mess = return_mess(200, f"Not found any. Here are some suggestions: {suggestion}")
        return mess
    else:
        mess = return_mess(200, f"Found {len(info.keys())} beer(s). List of beers with your suggest tagline: {info}")
        return mess

@task
def search_food_pairing(food):
    info = shopCart_app.info_by_foodpairing(food)
    if info == {}:
        suggestion = shopCart_app.food_suggestion()
        mess = return_mess(200, f"Not found any. Here are some suggestions: {suggestion}")
        return mess
    else:
        mess = return_mess(200, f"Found {len(info.keys())} beer(s). List of beers with your suggest tagline: {info}")
        return mess

@app.route('/<string:filter>/<string:keyword>',methods = ['GET'])
def big_search(filter, keyword):
    type_list = shopCart_app.return_type_list()
    if filter in type_list:
        pass
    else:
        mess = return_mess(404, f"Filter is not valid. Here are valid filters: {type_list}")
        return mess
    
    mess = tasks['search_' + filter](keyword)

    return mess
        
if __name__ == "__main__":
    app.run(debug = True, host="0.0.0.0", port=args.port)