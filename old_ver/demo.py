import os
import json
import glob
import logging
import argparse
from flask import Flask, request, jsonify

logger = logging.getLogger(__name__)
app = Flask(__name__)

parser = argparse.ArgumentParser("Hybrid gateway config")
parser.add_argument("--port", type=int, default=False, required=True)

@app.route("/", methods = ["GET"]) 
def home(): 
    if(request.method == "GET"): 
        data = "hello world"
        return jsonify({"data": data})

def return_mess(code, mes):
    return jsonify({"message": f"{mes}","status":code})

class shopCart():
    def __init__(self):
        self.root = "./"
        self.item_path = os.path.join(self.root, "beers.json")
        self.n_count = 0
        self.name_list = []

        f = open(self.item_path, "r")
        self.all_config = json.loads(f.read())

    def init_config(self, item_path):
        self.n_count = len(self.all_config)
        
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            self.name_list.append(db_name)

    def check_id(self, id):
        if id <= 25 and id >= 0:
            return True
        else:
            return False
    
    def check_name(self, name):
        self.init_config(self.item_path)
        if name in self.name_list:
            return True
        else:
            return False

    def get_name_list(self):
        self.init_config(self.item_path)
        return self.name_list
    
    def info_by_id(self, id):
        info = self.all_config[id - 1]
        return info

    def info_by_name(self, name):
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            if name == db_name:
                return content
            else:
                pass

    def info_by_price(self, price):
        return_list = []
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            db_price = content["price"]
            if int(price) == int(db_price):
                return_list.append(db_name)
        
        return return_list

    def price_range(self):
        return_range = []
        for i, content in enumerate(self.all_config):
            db_price = content["price"]
            return_range.append(db_price)

        return [min(return_range), max(return_range)]

    def info_by_tagline(self, tagline):
        list_content = {}
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            db_tagline = content["tagline"]
            for j, temp in enumerate(db_tagline.split()):
                if tagline == temp.strip(".") or tagline == temp:
                    list_content[i] = content
        
        return list_content

    def tagline_suggestion(self):
        return_suggestion = []
        for i, content in enumerate(self.all_config):
            db_tagline = content["tagline"]
            return_suggestion.append(db_tagline)

        return return_suggestion
    
    def info_by_foodpairing(self, food):
        list_content = {}
        for i, content in enumerate(self.all_config):
            db_name = content["name"]
            db_food = content["food_pairing"]
            for j, temp in enumerate(db_food):
                keyword = temp.split()
                if (food == vocab for vocab in keyword):
                    list_content[i] = content
        
        return list_content

    def food_suggestion(self):
        return_suggestion = []
        for i, content in enumerate(self.all_config):
            db_tagline = content["food_pairing"]
            return_suggestion.append(db_tagline)

        return return_suggestion

    def run(self):
        self.init_config(self.item_path)


shopCart_app = shopCart()
args = parser.parse_args()

@app.route('/id/<string:id>',methods = ['GET'])
def search_id(id):
    id = int(id)
    if shopCart_app.check_id(id):
        pass
    else:
        mess = return_mess(400, "ID is not valid.")
        return mess
    
    info = shopCart_app.info_by_id(id)

    mess = return_mess(200, info)
    return mess

@app.route('/name/<string:name>',methods = ['GET'])
def search_name(name):
    if shopCart_app.check_name(name):
        pass
    else:
        name_list = shopCart_app.get_name_list()
        mess = return_mess(400, f"Name is not valid. List of available beer: {name_list}")
        return mess
    
    info = shopCart_app.info_by_name(name)

    mess = return_mess(200, info)
    return mess

@app.route('/price/<string:price>',methods = ['GET'])
def search_price(price):
    price = float(price)
    info = shopCart_app.info_by_price(price)
    if info == []:
        price_range = shopCart_app.price_range()
        mess = return_mess(200, f"Here is the price range: {price_range}")
        return mess
    else:
        mess = return_mess(200, f"List of beers with your suggest price: {info}")
        return mess

@app.route('/tagline/<string:tagline>',methods = ['GET'])
def search_tagline(tagline):
    info = shopCart_app.info_by_tagline(tagline)
    if info == {}:
        suggestion = shopCart_app.tagline_suggestion()
        mess = return_mess(200, f"Not found any. Here are some suggestions: {suggestion}")
        return mess
    else:
        mess = return_mess(200, f"Found {len(info.keys())} beer(s). List of beers with your suggest tagline: {info}")
        return mess

@app.route('/food/<string:food>',methods = ['GET'])
def search_food(food):
    info = shopCart_app.info_by_foodpairing(food)
    if info == {}:
        suggestion = shopCart_app.food_suggestion()
        mess = return_mess(200, f"Not found any. Here are some suggestions: {suggestion}")
        return mess
    else:
        mess = return_mess(200, f"Found {len(info.keys())} beer(s). List of beers with your suggest tagline: {info}")
        return mess


if __name__ == "__main__":
    app.run(debug = True, host="0.0.0.0", port=args.port)